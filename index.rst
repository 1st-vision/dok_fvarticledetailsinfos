.. |label| replace:: Anzeige von Informationen auf ADS
.. |snippet| replace:: FvArticleDetailsInfos
.. |Author| replace:: 1st Vision GmbH
.. |minVersion| replace:: 5.6.0
.. |maxVersion| replace:: 5.6.9
.. |version| replace:: 1.1.0
.. |php| replace:: 7.4


|label|
============

.. sectnum::

.. contents:: Inhaltsverzeichnis



Überblick
---------
:Author: |Author|
:PHP: |php|
:Kürzel: |snippet|
:getestet für Shopware-Version: |minVersion| bis |maxVersion|
:Version: |version|

Beschreibung
------------
Auf der Artikeldetailseite wird die Artikelnummer des Grundartikels ausgeblendet, erst wenn eine Variante ausgewählt wurde, wird die Artikelnummer eingeblendet.
Mit Hilfe eines Schalter kann man in der Kaufabwicklung die Artikel hinzufügen-Funktion aktivieren und deaktivieren.


Backend
-------

.. image:: FvArticleDetailsInfos1.png

:Anzeige Artikelnummer bei Bestellabschluss: Hier können Sie das Eingabefeld aktivieren oder deaktivieren

:Liefertext ausblenden: Auf der ADS und in der Kaufabwicklen kann man nunden Liefertext ausblenden, wenn gewünscht

Textbausteine
_____________
keine

technische Beschreibung
------------------------
keine

Modifizierte Template-Dateien
-----------------------------
:/checkout/cart_footer.tpl:
:/detail/content/buy_container.tpl:
:/detail/data.tpl:
:/plugins/index/delivery_informations.tpl:


